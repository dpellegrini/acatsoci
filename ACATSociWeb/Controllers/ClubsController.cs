﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ACATSociWeb.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ACATSociWeb.Models;

namespace ACATSociWeb.Controllers
{
  public class ClubsController : Controller
  {
    private readonly ACATSociWebContext _context;
    private readonly IAppContext _iAppContext;

    private dynamic TuttiMembri { get; }

    public ClubsController(ACATSociWebContext context, IAppContext i_AppContext)
    {
      _context = context;
      _iAppContext = i_AppContext;

      IQueryable<string> sociQuery = from m in _context.Socio
                                     orderby m.Cognome
                                     select m.Nome + @" " + m.Cognome;

      try
      {
        TuttiMembri = new SelectList(sociQuery.Distinct().ToList());
      }
      catch (Exception e)
      {
        Console.WriteLine(e);
      }
    }

    // GET: Clubs
    public async Task<IActionResult> Index()
    {
      if (!_iAppContext.IsAuthenticated)
      {
        return RedirectToAction("Authorize", "Login");
      }

      var list = await _context.ClubTorinoEst.ToListAsync();

      var clubsViewModel = new ClubsViewModel();
      clubsViewModel.ListClubsViewModel = new List<ClubViewModel>();
      if (list.Any())
      {
        list.ForEach(i => clubsViewModel.ListClubsViewModel.Add(new ClubViewModel
        {
          CanEdit = _iAppContext.IsAdmin,
          CAT = i
        }));
      }
      clubsViewModel.CanCreateNewClub = _iAppContext.IsAdmin;

      return View(clubsViewModel);
    }

    // GET: Clubs/Details/5
    public async Task<IActionResult> Details(int? id)
    {
      if (id == null)
      {
        return NotFound();
      }

      var clubTorinoEst = await _context.ClubTorinoEst
          .FirstOrDefaultAsync(m => m.Id == id);
      if (clubTorinoEst == null)
      {
        return NotFound();
      }

      var clubVM = new ClubViewModel
      {
        CanEdit = _iAppContext.IsAdmin,
        CAT = clubTorinoEst,
        Club = clubTorinoEst.Club.ToString()
      };

      return View(clubVM);
    }

    // GET: Clubs/Create
    public IActionResult Create()
    {
      ViewBag.TuttiMembri = TuttiMembri;
      return View();
    }

    // POST: Clubs/Create
    // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
    // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Create([Bind("Id,Club,Presidente,Segretario,Tesoriere,ServitoreInsegnante,NumeroTelefonico,Sede,GiornoIncontri,OraInizio,OraFine")] ClubTorinoEst clubTorinoEst)
    {
      if (ModelState.IsValid)
      {
        _context.Add(clubTorinoEst);
        await _context.SaveChangesAsync();
        return RedirectToAction(nameof(Index));
      }
      ViewBag.TuttiMembri = TuttiMembri;
      return View(clubTorinoEst);
    }

    // GET: Clubs/Edit/5
    public async Task<IActionResult> Edit(int? id)
    {
      if (id == null)
      {
        return NotFound();
      }

      var clubTorinoEst = await _context.ClubTorinoEst.FindAsync(id);
      if (clubTorinoEst == null)
      {
        return NotFound();
      }
      ViewBag.TuttiMembri = TuttiMembri;
      return View(clubTorinoEst);
    }

    // POST: Clubs/Edit/5
    // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
    // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Edit(int id, [Bind("Id,Club,Presidente,Segretario,Tesoriere,ServitoreInsegnante,NumeroTelefonico,Sede,GiornoIncontri,OraInizio,OraFine")] ClubTorinoEst clubTorinoEst)
    {
      if (id != clubTorinoEst.Id)
      {
        return NotFound();
      }

      if (ModelState.IsValid)
      {
        try
        {
          _context.Update(clubTorinoEst);
          await _context.SaveChangesAsync();
        }
        catch (DbUpdateConcurrencyException)
        {
          if (!ClubTorinoEstExists(clubTorinoEst.Id))
          {
            return NotFound();
          }
          else
          {
            throw;
          }
        }
        return RedirectToAction(nameof(Index));
      }
      return View(clubTorinoEst);
    }

    // GET: Clubs/Delete/5
    public async Task<IActionResult> Delete(int? id)
    {
      if (id == null)
      {
        return NotFound();
      }

      var clubTorinoEst = await _context.ClubTorinoEst
          .FirstOrDefaultAsync(m => m.Id == id);
      if (clubTorinoEst == null)
      {
        return NotFound();
      }

      return View(clubTorinoEst);
    }

    // POST: Clubs/Delete/5
    [HttpPost, ActionName("Delete")]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> DeleteConfirmed(int id)
    {
      var clubTorinoEst = await _context.ClubTorinoEst.FindAsync(id);
      _context.ClubTorinoEst.Remove(clubTorinoEst);
      await _context.SaveChangesAsync();
      return RedirectToAction(nameof(Index));
    }

    private bool ClubTorinoEstExists(int id)
    {
      return _context.ClubTorinoEst.Any(e => e.Id == id);
    }

  }
}
