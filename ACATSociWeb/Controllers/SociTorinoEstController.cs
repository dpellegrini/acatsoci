﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ACATSociWeb.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ACATSociWeb.Models;
using Microsoft.Extensions.FileProviders;

namespace ACATSociWeb.Controllers
{
  public class SociTorinoEstController : Controller
  {
    private readonly ACATSociWebContext _context;
    private readonly IFileProvider _iFileProvider;
    private readonly IAppContext _iAppContext;
    private SelectList TuttiClub { get; }

    public SociTorinoEstController(ACATSociWebContext context, IFileProvider i_FileProvider, IAppContext i_AppContext)
    {
      _context = context;
      _iFileProvider = i_FileProvider;
      _iAppContext = i_AppContext;

      IQueryable<string> clubsQuery = from m in _context.ClubTorinoEst
        orderby m.Club
        select m.Club;

      try
      {
        TuttiClub = new SelectList(clubsQuery.Distinct().ToList());
      }
      catch (Exception e)
      {
        Console.WriteLine(e);
      }
    }


    // GET: SociTorinoEst
    public IActionResult Index(string socioClub, string ruoloClub, string searchString)
    {
      if (!_iAppContext.IsAuthenticated)
      {
        return RedirectToAction("Authorize", "Login");
      }

      DateTimeOffset lastModifiedDate = _iFileProvider.GetFileInfo(@"Views\SociTorinoEst\Index.cshtml").LastModified;
      ViewData["LastModified"] = lastModifiedDate;

      var socioClubVM = GetSocioClubViewModel(socioClub, ruoloClub, searchString);

      return View(socioClubVM);
    }

    // GET: SociTorinoEst/Details/5
    public async Task<IActionResult> Details(int? id)
    {
      if (id == null)
      {
        return NotFound();
      }

      var socio = await _context.Socio
          .FirstOrDefaultAsync(m => m.Id == id);

      var clubTorinoEst = await _context.ClubTorinoEst.FirstOrDefaultAsync(m => m.Club == socio.CatDiAppartenenza);

      if (socio == null)
      {
        return NotFound();
      }

      var clubVM = new ClubViewModel
      {
        CanEdit = _iAppContext.IsAdmin || _iAppContext.AuthorizedUser.Id == socio.Id,
        Socio = socio,
        CAT = clubTorinoEst
      };

      return View(clubVM);
    }

    private SocioClubViewModel GetSocioClubViewModel(string socioClub, string ruoloClub, string searchString)
    {
      // Use LINQ to get list 
      IQueryable<string> clubsQuery = from m in _context.Socio
        orderby m.CatDiAppartenenza
        select m.CatDiAppartenenza;

      // Use LINQ to get list 
      IQueryable<RuoliSocio> ruoliQuery = from m in _context.Socio
        orderby m.RuoloAlCat
        select m.RuoloAlCat;

      var soci = from m in _context.Socio select m;

      if (!string.IsNullOrEmpty(searchString))
      {
        soci = soci.Where(s => s.Cognome.Contains(searchString) || s.Nome.Contains(searchString));
      }

      if (!string.IsNullOrEmpty(socioClub))
      {
        soci = soci.Where(s => s.CatDiAppartenenza.ToString() == socioClub);
      }

      if (!string.IsNullOrEmpty(ruoloClub))
      {
        soci = soci.Where(s => s.RuoloAlCat.ToString() == ruoloClub);
      }

      try
      {
        var cc = soci.ToList();
      }
      catch (Exception e)
      {
        soci = new EnumerableQuery<Socio>(new List<Socio>());
      }

      var socioClubVM = new SocioClubViewModel
      {
        Clubs = new SelectList(clubsQuery.Distinct().ToList()),
        Ruoli = new SelectList(ruoliQuery.Distinct().ToList()),
        Soci = soci.ToList(),
        SocioClub = socioClub?? string.Empty,
        RuoloClub = ruoloClub?? string.Empty,
        SearchString = searchString?? string.Empty,
        CanEdit = _iAppContext.IsAdmin,
        AuthenticatedUser = _iAppContext.AuthorizedUser
      };

      return socioClubVM;
    }

    // GET: SociTorinoEst/Create
    public IActionResult Create()
    {
      ViewBag.TuttiClub = TuttiClub;
      return View();
    }

    // GET: SociTorinoEst/Register
    public IActionResult Register()
    {
      ViewBag.TuttiClub = TuttiClub;
      return View();
    }

    // POST: SociTorinoEst/Register
    // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
    // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Register([Bind("Id,Cognome,Nome,Qualifica,Nazionalita,ComuneDiNascita_Stato,Provincia,DataDiNascita,IndirizzoDiResidenza,ComuneDiResidenza,ProvinciaDiResidenza,CAP,Cellulare,Email,CodiceFiscale,CatDiAppartenenza,DataIscrizioneCat,RuoloAlCat")] Socio socio)
    {
      if (ModelState.IsValid)
      {
        var exists = await _context.Socio.FirstOrDefaultAsync(m => m.CodiceFiscale.Equals(socio.CodiceFiscale));
        if (exists == null)
        {
          _context.Add(socio);
          await _context.SaveChangesAsync();

          var newSocio = await _context.Socio.FirstOrDefaultAsync(m => m.Nome.Equals(socio.Nome, StringComparison.CurrentCultureIgnoreCase) && m.Cognome.Equals(socio.Cognome, StringComparison.CurrentCultureIgnoreCase));
          if (newSocio != null)
          {
            _iAppContext.AuthorizedUser = newSocio;
            _iAppContext.IsAuthenticated = true;
            _iAppContext.IsAdmin = false;
            return RedirectToAction("Details", new{Id = newSocio.Id});
          }
        }
      }
      ViewBag.TuttiClub = TuttiClub;
      return View(socio);
    }

    // POST: SociTorinoEst/Create
    // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
    // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Create([Bind("Id,Cognome,Nome,Qualifica,Nazionalita,ComuneDiNascita_Stato,Provincia,DataDiNascita,IndirizzoDiResidenza,ComuneDiResidenza,ProvinciaDiResidenza,CAP,Cellulare,Email,CodiceFiscale,CatDiAppartenenza,DataIscrizioneCat,RuoloAlCat")] Socio socio)
    {
      if (ModelState.IsValid)
      {
        var exists = await _context.Socio.FirstOrDefaultAsync(m => m.CodiceFiscale.Equals(socio.CodiceFiscale));
        if (exists == null)
        {
          _context.Add(socio);
          await _context.SaveChangesAsync();
          return RedirectToAction(nameof(Index));
        }
      }
      ViewBag.TuttiClub = TuttiClub;
      return View(socio);
    }

    // GET: SociTorinoEst/Edit/5
    public async Task<IActionResult> Edit(int? id)
    {
      if (id == null)
      {
        return NotFound();
      }

      var socio = await _context.Socio.FindAsync(id);
      if (socio == null)
      {
        return NotFound();
      }
      ViewBag.TuttiClub = TuttiClub;
      return View(socio);
    }

    // POST: SociTorinoEst/Edit/5
    // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
    // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Edit(int id, [Bind("Id,Cognome,Nome,Qualifica,Nazionalita,ComuneDiNascita_Stato,Provincia,DataDiNascita,IndirizzoDiResidenza,ComuneDiResidenza,ProvinciaDiResidenza,CAP,Cellulare,Email,CodiceFiscale,CatDiAppartenenza,DataIscrizioneCat,RuoloAlCat")] Socio socio)
    {
      if (id != socio.Id)
      {
        return NotFound();
      }

      if (ModelState.IsValid)
      {
        try
        {
          _context.Update(socio);
          await _context.SaveChangesAsync();
        }
        catch (DbUpdateConcurrencyException)
        {
          if (!SocioExists(socio.Id))
          {
            return NotFound();
          }
          else
          {
            throw;
          }
        }
        return RedirectToAction(nameof(Index));
      }
      ViewBag.TuttiClub = TuttiClub;
      return View(socio);
    }

    // GET: SociTorinoEst/Delete/5
    public async Task<IActionResult> Delete(int? id)
    {
      if (id == null)
      {
        return NotFound();
      }

      var socio = await _context.Socio
          .FirstOrDefaultAsync(m => m.Id == id);
      if (socio == null)
      {
        return NotFound();
      }

      return View(socio);
    }

    // POST: SociTorinoEst/Delete/5
    [HttpPost, ActionName("Delete")]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> DeleteConfirmed(int id)
    {
      var socio = await _context.Socio.FindAsync(id);
      _context.Socio.Remove(socio);
      await _context.SaveChangesAsync();
      return RedirectToAction(nameof(Index));
    }

    private bool SocioExists(int id)
    {
      return _context.Socio.Any(e => e.Id == id);
    }

    public IActionResult ExportExcel(string socioClub, string ruoloClub, string searchString)
    {
      var vm = GetSocioClubViewModel(socioClub, ruoloClub, searchString);
      return View(vm);
    }
  }
}
