﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ACATSociWeb.Interfaces;
using ACATSociWeb.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Session;
using Microsoft.EntityFrameworkCore;

namespace ACATSociWeb.Controllers
{
  public class LoginController : Controller
  {
    private readonly ACATSociWebContext _context;
    private IAppContext _AppContext { get; set; }
    
    private dynamic TuttiMembri { get; }

    public LoginController(ACATSociWebContext context, IAppContext appContext)
    {
      _context = context;
      _AppContext = appContext;

      IQueryable<string> sociQuery = from m in _context.Socio
        orderby m.Cognome
        select m.Nome +  @"." + m.Cognome;

      try
      {
        TuttiMembri = sociQuery.Distinct().ToList();
      }
      catch (Exception e)
      {
        Console.WriteLine(e);
      }

    }

    public IActionResult Authorize()
    {
      return View();
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Authorize(AuthorizeModel i_Authorize)
    {
      if (ModelState.IsValid)
      {

        if (!i_Authorize.User.Contains("@acat.to.est", StringComparison.CurrentCultureIgnoreCase))
        {
          i_Authorize.AuthorizeErrorMessage = @"Utente non valido (es. nome.cognome@acat.to.est)";
          return View(i_Authorize);
        }

        var validPass = false;
        var validUser = false;

        _AppContext.IsAdmin = false;
        _AppContext.AuthorizedUser = null;
        _AppContext.IsAuthenticated = false;

        var user = i_Authorize.User.Split('@');
        var userName = user[0].Replace(" " , "").Split('.');

        if (_context.Socio.Any())
        {
          _AppContext.AuthorizedUser = await _context.Socio.FirstOrDefaultAsync(m =>
            m.Nome.Equals(userName[0], StringComparison.CurrentCultureIgnoreCase) &&
            m.Cognome.Equals(userName[1], StringComparison.CurrentCultureIgnoreCase));
          validUser = _AppContext.AuthorizedUser != null;
        }

        if (i_Authorize.Password.Equals(@"liberidiscegliere"))
        {
          _AppContext.IsAuthenticated = true;
          _AppContext.IsAdmin = false;
          validPass = true;
        }

        if (i_Authorize.Password.Equals(@"master"))
        {
          _AppContext.IsAuthenticated = true;
          _AppContext.IsAdmin = true;
          validPass = true;
        }
        if (validPass && _AppContext.IsAdmin)
        {
          return RedirectToAction("Index", "SociTorinoEst");
        }

        if (validPass && validUser)
        {
          return RedirectToAction("Index", "SociTorinoEst");
        }

        if (!validUser)
        {
          i_Authorize.AuthorizeErrorMessage = $"Utente {i_Authorize.User} non trovato";
        }
        else if (!validPass)
        {
          i_Authorize.AuthorizeErrorMessage = "Password invalida";
       
        }
        return View(i_Authorize);
      }
      return View(i_Authorize);
    }

    public IActionResult Logout()
    {
      _AppContext.IsAuthenticated = false;
      return RedirectToAction("Authorize", "Login");
    }
  }
}