﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ACATSociWeb.Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using ACATSociWeb.Models;
using AppContext = ACATSociWeb.Models.AppContext;

namespace ACATSociWeb
{
  public class Startup
  {
    private readonly IHostingEnvironment _iHostingEnvironment;

    public Startup(IConfiguration configuration, IHostingEnvironment iHostingEnvironment)
    {
      _iHostingEnvironment = iHostingEnvironment;
      Configuration = configuration;
    }

    public IConfiguration Configuration { get; }

    // This method gets called by the runtime. Use this method to add services to the container.
    public void ConfigureServices(IServiceCollection services)
    {
      services.AddSingleton(_iHostingEnvironment.ContentRootFileProvider);
      services.AddScoped<IClubsRepository, ClubsRepository>();
      services.AddSingleton<IAppContext, AppContext>();

      services.Configure<CookiePolicyOptions>(options =>
      {
        // This lambda determines whether user consent for non-essential cookies is needed for a given request.
        options.CheckConsentNeeded = context => true;
        options.MinimumSameSitePolicy = SameSiteMode.None;
      });

      services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

      //var connectStr = Configuration.GetConnectionString("ACATSociWebContext");
      services.AddDbContext<ACATSociWebContext>(options =>
              options.UseSqlServer(Configuration.GetConnectionString("ACATSociWebContext")));
    }

    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void Configure(IApplicationBuilder app, IHostingEnvironment env)
    {
      if (env.IsDevelopment())
      {
        app.UseDeveloperExceptionPage();
      }
      else
      {
        app.UseExceptionHandler("/Home/Error");
        // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
        app.UseHsts();
      }

      app.UseHttpsRedirection();
      app.UseStaticFiles();
      app.UseCookiePolicy();

      app.UseMvc(routes =>
      {
        routes.MapRoute(
                  name: "default",
                  //template: "{controller=SociTorinoEst}/{action=Index}/{id?}");
                  template: "{controller=Login}/{action=Authorize}/{id?}");
      });
    }
  }
}
