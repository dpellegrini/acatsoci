﻿using System.Collections.Generic;
using ACATSociWeb.Models;

namespace ACATSociWeb.Interfaces
{
  public interface IClubsRepository
  {
    IEnumerable<ClubTorinoEst> Clubs { get; }

    IEnumerable<Socio> Soci { get; }
  }
}