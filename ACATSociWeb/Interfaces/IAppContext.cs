﻿using ACATSociWeb.Models;

namespace ACATSociWeb.Interfaces
{
  public interface IAppContext
  {
    bool IsAdmin { get; set; } 
    bool IsAuthenticated { get; set; }
    Socio AuthorizedUser { get; set; }
  }
}