﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ACATSociWeb.Interfaces;
using Microsoft.EntityFrameworkCore;
using ACATSociWeb.Models;

namespace ACATSociWeb.Models
{
    public class ACATSociWebContext : DbContext
    {
        public ACATSociWebContext (DbContextOptions<ACATSociWebContext> options)
            : base(options)
        {
        }

        public DbSet<ACATSociWeb.Models.Socio> Socio { get; set; }

        public DbSet<ACATSociWeb.Models.ClubTorinoEst> ClubTorinoEst { get; set; }
    }
}
