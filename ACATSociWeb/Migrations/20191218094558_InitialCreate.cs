﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ACATSociWeb.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ClubTorinoEst",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Club = table.Column<string>(nullable: false),
                    Presidente = table.Column<string>(nullable: true),
                    Segretario = table.Column<string>(nullable: true),
                    Tesoriere = table.Column<string>(nullable: true),
                    ServitoreInsegnante = table.Column<string>(nullable: true),
                    NumeroTelefonico = table.Column<string>(nullable: true),
                    Sede = table.Column<string>(nullable: false),
                    GiornoIncontri = table.Column<string>(nullable: true),
                    OraInizio = table.Column<DateTime>(nullable: true),
                    OraFine = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClubTorinoEst", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Socio",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Cognome = table.Column<string>(nullable: false),
                    Nome = table.Column<string>(nullable: false),
                    Qualifica = table.Column<string>(nullable: true),
                    Nazionalita = table.Column<string>(nullable: true),
                    ComuneDiNascita_Stato = table.Column<string>(nullable: true),
                    Provincia = table.Column<string>(nullable: true),
                    DataDiNascita = table.Column<DateTime>(nullable: true),
                    IndirizzoDiResidenza = table.Column<string>(nullable: true),
                    ComuneDiResidenza = table.Column<string>(nullable: true),
                    ProvinciaDiResidenza = table.Column<string>(nullable: true),
                    CAP = table.Column<string>(nullable: true),
                    Cellulare = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    CodiceFiscale = table.Column<string>(nullable: true),
                    CatDiAppartenenza = table.Column<string>(nullable: false),
                    DataIscrizioneCat = table.Column<DateTime>(nullable: true),
                    RuoloAlCat = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Socio", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ClubTorinoEst");

            migrationBuilder.DropTable(
                name: "Socio");
        }
    }
}
