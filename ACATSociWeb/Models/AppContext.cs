﻿using ACATSociWeb.Interfaces;

namespace ACATSociWeb.Models
{
  public class AppContext : IAppContext
  {
    public bool IsAdmin { get; set; } = true;
    public bool IsAuthenticated { get; set; }
    public Socio AuthorizedUser { get; set; }
  }
}