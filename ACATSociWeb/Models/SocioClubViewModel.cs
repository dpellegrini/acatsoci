﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace ACATSociWeb.Models
{
  public class SocioClubViewModel
  {
    public bool CanEdit { get; set; }
    public List<Socio> Soci { get; set; }
    public List<RuoliSocio> RuoliSocio { get; set; }
    public SelectList Clubs { get; set; }
    public SelectList Ruoli { get; set; }
    public string SocioClub { get; set; }
    public string RuoloClub { get; set; }
    public string SearchString { get; set; }
    public Socio AuthenticatedUser { get; set; }
  }
}