﻿namespace ACATSociWeb.Models
{
  public enum FrequenzaCat
  {
    Normale,
    Saltuaria,
    UnaVoltaAlMese,
    Raramente,
    NonFrequenta
  }
}