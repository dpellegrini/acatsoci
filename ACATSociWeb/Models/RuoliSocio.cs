﻿using System.ComponentModel.DataAnnotations;

namespace ACATSociWeb.Models
{
  public enum RuoliSocio
  {
    Socio,
    [Display(Name = "Servitore Insegnante")]
    ServitoreInsegnante,
    Segretario,
    Tesoriere,
    Presidente
  }
}