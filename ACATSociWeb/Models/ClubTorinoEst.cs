﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ACATSociWeb.Models
{
  public class ClubTorinoEst
  {
    public int Id { get; set; }
    [Required]
    public string Club { get; set; }
    [DataType(DataType.Text)]
    public string Presidente { get; set; }
    [DataType(DataType.Text)]
    public string Segretario { get; set; }
    [DataType(DataType.Text)]
    public string Tesoriere { get; set; }
    [Display(Name = "Servitore Insegnante")]
    [DataType(DataType.Text)]
    public string ServitoreInsegnante { get; set; }
    [Display(Name = "Telefono/SMS")]
    [DataType(DataType.Text)]
    public string NumeroTelefonico { get; set; }
    [Required]
    public string Sede { get; set; }
    [Display(Name = "Giorno Incontri")]
    [DataType(DataType.Text)]
    public string GiornoIncontri { get; set; }
    [DisplayFormat(DataFormatString = "{0:HH:mm}", ApplyFormatInEditMode = true)]
    [Display(Name = "Ora Inizio")]
    public Nullable<DateTime>  OraInizio { get; set; }
    [DisplayFormat(DataFormatString = "{0:HH:mm}", ApplyFormatInEditMode = true)]
    [Display(Name = "Ora Fine")]
    public Nullable<DateTime>  OraFine { get; set; }
  }
}
