﻿using System.Collections.Generic;

namespace ACATSociWeb.Models
{
  public class ClubsViewModel
  {
    public bool CanCreateNewClub { get; set; }
    public List<ClubViewModel> ListClubsViewModel { get; set; }
  }
}