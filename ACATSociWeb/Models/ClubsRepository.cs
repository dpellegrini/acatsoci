﻿using System.Collections.Generic;
using ACATSociWeb.Interfaces;

namespace ACATSociWeb.Models
{
  public class ClubsRepository : IClubsRepository
  {
    private ACATSociWebContext _acatSociWebContext;
    public ClubsRepository(ACATSociWebContext i_Context)
    {
      _acatSociWebContext = i_Context;
    }

    public IEnumerable<ClubTorinoEst> Clubs => _acatSociWebContext.ClubTorinoEst;
    public IEnumerable<Socio> Soci => _acatSociWebContext.Socio;
  }
}