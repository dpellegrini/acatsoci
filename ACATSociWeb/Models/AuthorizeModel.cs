﻿using System.ComponentModel.DataAnnotations;

namespace ACATSociWeb.Models
{
  public class AuthorizeModel
  {
    [Required(ErrorMessage = "Utente e` richiesto")]
    public string User { get; set; }

    [Required(ErrorMessage = "Password e` richiesta")]
    [DataType(DataType.Password)]

    public string Password { get; set; }
    public string AuthorizeErrorMessage { get; set; }
  }
}