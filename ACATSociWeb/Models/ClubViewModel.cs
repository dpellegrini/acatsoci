﻿namespace ACATSociWeb.Models
{
  public class ClubViewModel
  {
    public bool CanEdit { get; set; }
    public ClubTorinoEst CAT { get; set; }
    public string Club { get; set; }
    public Socio Socio { get; set; }
  }
}