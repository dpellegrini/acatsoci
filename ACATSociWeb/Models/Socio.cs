﻿using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;

namespace ACATSociWeb.Models
{
  public class Socio
  {
    public int Id { get; set; }
    [DataType(DataType.Text)]
    [Required(ErrorMessage = "Il Cognome e` un dato richiesto")]
    public string Cognome { get; set; }
    [DataType(DataType.Text)]
    [Required(ErrorMessage = "Il Nome e` un dato richiesto")]
    public string Nome { get; set; }
    [Display(Name = "Professione")]
    [DataType(DataType.Text)]
    public string Qualifica { get; set; }
    [DataType(DataType.Text)]
    public string Nazionalita { get; set; }
    [DataType(DataType.Text)]
    [Display(Name = "Comune Nascita (o Stato)")]
    public string ComuneDiNascita_Stato { get; set; }
    [DataType(DataType.Text)]
    public string Provincia { get; set; }
    [DisplayFormat(DataFormatString = "{0:dd-MM-yy}", ApplyFormatInEditMode = false)]
    [DataType(DataType.Date)]
    [Display(Name = "Data di Nascita")]
    public Nullable<DateTime>  DataDiNascita { get; set; }
    [Display(Name = "Indirizzo")]
    [DataType(DataType.Text)]
    public string IndirizzoDiResidenza { get; set; }
    [Display(Name = "Comune")]
    [DataType(DataType.Text)]
    public string ComuneDiResidenza { get; set; }
    [Display(Name = "Provincia")]
    [DataType(DataType.Text)]
    public string ProvinciaDiResidenza { get; set; }
    [DataType(DataType.PostalCode)]
    public string CAP { get; set; }
    [DataType(DataType.PhoneNumber)]
    public string Cellulare { get; set; }
    [Display(Name = "EMAIL")]
    [DataType(DataType.EmailAddress)]
    public string Email { get; set; }
    [Display(Name = "CF")]
    [Required(ErrorMessage = "Il Codice Fiscale e` un dato richiesto")]
    [DataType(DataType.Text)]
    public string CodiceFiscale { get; set; }
    [Required]
    [Display(Name = "CAT")]
    public string CatDiAppartenenza { get; set; }
    [Display(Name = "Iscrizione")]
    [DisplayFormat(DataFormatString = "{0:dd-MM-yy}", ApplyFormatInEditMode = false)]
    [DataType(DataType.Date)]
    public Nullable<DateTime> DataIscrizioneCat { get; set; }
    [Display(Name = "Ruolo")]
    public RuoliSocio RuoloAlCat { get; set; }
  }
}