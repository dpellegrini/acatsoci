﻿using System.Collections.ObjectModel;
using ACATSoci.Core;
using ACATSoci.Core.Models;
using Microsoft.Office.Interop.Excel;

namespace ACATSoci.Models
{
  public class WorksheetModel : SuperSimpleModelBase
  {
    private readonly Excel _iExcel;
    private readonly Worksheet _iWorksheet;

    public string Name
    {
      get => Get<string>();
      set => Set(value);
    }
    
    public ObservableCollection<string> HeadersCollection
    {
      get => Get<ObservableCollection<string>>();
      set => Set(value);
    }

    public WorksheetModel(Excel iExcelObject, Worksheet i_Worksheet, int i_Index)
    {
      _iExcel = iExcelObject;
      _iWorksheet = i_Worksheet;

      Name = i_Worksheet.Name;
      var headers = _iExcel.LoadHeaders(_iWorksheet);
      HeadersCollection = new ObservableCollection<string>();
      headers.ForEach(i_Header => HeadersCollection.Add(i_Header));
    }
  }
}