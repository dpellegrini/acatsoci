﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Windows.Documents;
using Microsoft.Office.Interop.Excel;
using Microsoft.Win32;

namespace ACATSoci.Core
{
  public class Excel
  {
    private _Application ExcelApp { get; set; } = new Application();

    private Workbook ActiveWorkbook { get; set; }
    public Worksheet ActiveWorksheet { get; set; }

    public void Open(string i_FilePath)
    {
      ActiveWorkbook = ExcelApp?.Workbooks.Open(i_FilePath);
    }

    public void ActivateWorksheet(string i_WorksheetName)
    {
      if (ActiveWorkbook == null) return;
      foreach (var activeWorkbookWorksheet in ActiveWorkbook.Worksheets)
      {
        if (activeWorkbookWorksheet is Worksheet ws)
        {
          if (ws.Name.Contains(i_WorksheetName))
          {
            ActiveWorksheet = ws;
            break;
          }
        }
      }
    }

    public List<Worksheet> GetAvailableWorksheets()
    {
      if (ActiveWorkbook == null) return null;
      var ret = new List<Worksheet>();
      foreach (var activeWorkbookWorksheet in ActiveWorkbook.Worksheets)
      {
        if (activeWorkbookWorksheet is Worksheet ws)
        {
          Debug.WriteLine(ws.Name);
          ret.Add(ws);
        }
      }

      return ret;
    }

    public string ReadCell(int i_Row, int i_Col)
    {
      if (i_Row < 1) i_Row = 1;
      if (i_Col < 1) i_Col = 1;

      if (ActiveWorksheet.Cells[i_Row, i_Col].Value2 != null)
        return ActiveWorksheet.Cells[i_Row, i_Col].Value2.ToString();
      
      return string.Empty;
    }

    public List<string> LoadHeaders(Worksheet i_Worksheet = null)
    {
      var ret = new List<string>();

      Worksheet ws = i_Worksheet ?? ActiveWorksheet;
      if (ws == null) return ret;
      int row = 1, col = 1;

      do
      {
        if (ws.Cells[row, col].Value2 != null)
        {
          ret.Add(ReadCell(row, col));
          Debug.WriteLine(ret.Last());
          col++;
        }
      } while (ws.Cells[row,col].Value2 != null);

      return ret;
    }

    public List<string> Values(int i_Row = 1)
    {
      var ret = new List<string>();

      if (ActiveWorksheet == null) return ret;
      int col = 1;
      i_Row++;

      do
      {
        if (ActiveWorksheet.Cells[i_Row, col].Value2 != null)
        {
          ret.Add(ReadCell(i_Row, col));
          Debug.WriteLine(ret.Last());
          col++;
        }
      } while (ActiveWorksheet.Cells[i_Row, col].Value2 != null);


      return ret;

    }

    public void Close()
    {
      ActiveWorkbook.Close();
    }


  }
}