using System;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Linq.Expressions;

namespace ACATSoci.Core.Models
{
  /// <summary>
  /// Very basic implementation only of INotifyPropertyChanged.
  /// Absolute fastest observable model (1M updates in 449 ms)
  /// </summary>
  [Serializable]
  public class NotifyPropertyObject : INotifyPropertyChanged
  {
    protected void OnPropertyChanged(string i_Name)
    {
      PropertyChangedEventHandler handler = PropertyChanged;
      handler?.Invoke(this, new PropertyChangedEventArgs(i_Name));
    }

    /// <summary>
    /// Provides access to the PropertyChanged event handler to derived classes.
    /// </summary>
    protected PropertyChangedEventHandler PropertyChangedHandler => PropertyChanged;

    /// <summary>
    /// Raises the PropertyChanged event if needed.
    /// </summary>
    /// <remarks>If the propertyName parameter
    /// does not correspond to an existing property on the current class, an
    /// exception is thrown in DEBUG configuration only.</remarks>
    /// <param name="i_PropertyName">The name of the property that
    /// changed.</param>
    [SuppressMessage("Microsoft.Design", "CA1030:UseEventsWhereAppropriate",
      Justification = @"This cannot be an event")]
    public virtual void RaisePropertyChanged(string i_PropertyName)
    {
      var handler = PropertyChanged;
      handler?.Invoke(this, new PropertyChangedEventArgs(i_PropertyName));
    }

    /// <summary>
    /// Raises the PropertyChanged event if needed.
    /// </summary>
    /// <typeparam name="T">The type of the property that
    /// changed.</typeparam>
    /// <param name="i_PropertyExpression">An expression identifying the property
    /// that changed.</param>
    [SuppressMessage("Microsoft.Design", "CA1030:UseEventsWhereAppropriate",
      Justification = @"This cannot be an event")]
    [SuppressMessage(
      "Microsoft.Design",
      "CA1006:GenericMethodsShouldProvideTypeParameter",
      Justification = @"This syntax is more convenient than other alternatives.")]
    protected virtual void RaisePropertyChanged<T>(Expression<Func<T>> i_PropertyExpression)
    {
      if (i_PropertyExpression == null)
      {
        return;
      }

      var handler = PropertyChanged;

      if (handler != null)
      {
        if (i_PropertyExpression.Body is MemberExpression body)
        {
          if (body.Expression is ConstantExpression expression) handler(expression.Value, new PropertyChangedEventArgs(body.Member.Name));
        }
      }
    }

   
    [field:NonSerialized]
    public virtual event PropertyChangedEventHandler PropertyChanged;
  }
}

