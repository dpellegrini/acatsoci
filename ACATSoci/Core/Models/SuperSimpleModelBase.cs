﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;

namespace ACATSoci.Core.Models
{
  public class SuperSimpleModelBase : NotifyPropertyObject
  {
    private Dictionary<string, object> _container;

    private Dictionary<string, object> Container
    {
      get
      {
        if (_container == null)
        {
          _container = new Dictionary<string, object>();
        }
        return _container;
      }
    }

    protected T Get<T>([CallerMemberName] string i_ParamName = null)
    {
      if (i_ParamName != null && Container.ContainsKey(i_ParamName))
      {
        return (T)Container[i_ParamName];
      }
      return default(T);
    }

    protected T Get<T>(Expression<Func<T>> i_Expression)
    {
      // ReSharper disable once ExplicitCallerInfoArgument
      return Get<T>(PropertyName(i_Expression));
    }

    protected bool Set<T>(T i_Value, bool i_RaisePropChanged = true, [CallerMemberName] string i_PropName = null)
    {
      return Set(i_PropName, i_Value, i_RaisePropChanged);
    }

    protected virtual bool Set<T>(string i_ParamName, T i_Value, bool i_RaisePropertyChanged)
    {
      if (Container.ContainsKey(i_ParamName))
      {
        var current = Container[i_ParamName];
        if ((current == null && i_Value == null) || (current != null && current.Equals(i_Value)))
        {
          return false;
        }

        Container[i_ParamName] = i_Value;
      }
      else
      {
        Container.Add(i_ParamName, i_Value);
      }

      if (i_RaisePropertyChanged)
      {
        RaisePropertyChanged(i_ParamName);
      }

      return true;
    }

    protected bool Set<T>(Expression<Func<T>> i_Expression, T i_Value, bool i_RaisePropChanged = true)
    {
      return Set(PropertyName(i_Expression), i_Value, i_RaisePropChanged);
    }

    protected static string PropertyName<T>(Expression<Func<T>> i_Expression)
    {
      if (i_Expression == null)
      {
        return null;
      }

      var memberExpression = i_Expression.Body as MemberExpression;

      if (memberExpression == null)
      {
        throw new ArgumentException(@"expression must be a property expression");
      }

      return memberExpression.Member.Name;
    }

    public virtual void Load() { }
    public virtual void UnLoad() { }
  }
}