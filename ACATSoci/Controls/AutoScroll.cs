﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Interactivity;

namespace TestGame.Controls
{
  public class AutoScroll : Behavior<ScrollViewer>
  {
    protected static int Items { get; set; }

    public int ItemsCount
    {
      get { return (int)GetValue(ItemsCountProperty); }
      set { SetValue(ItemsCountProperty, value); }
    }

    // Using a DependencyProperty as the backing store for MyProperty.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty ItemsCountProperty =
        DependencyProperty.Register("ItemsCount", typeof(int), typeof(AutoScroll), new PropertyMetadata(CollectionChanged));

    private static void CollectionChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
      var items = (int) e.NewValue;
      {
        if (items > Items) // Added
        {
          Viewer?.ScrollToBottom();
        }

        Items = items;
      }
    }

    public static ScrollViewer Viewer {get; set;}
  
    protected override void OnAttached()
    {
      base.OnAttached();
      Viewer = AssociatedObject;
      Items = ItemsCount;
    }
  }       
}