﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Reflection;
using System.Windows;
using System.Windows.Shapes;
using ACATSoci.Core;
using ACATSoci.Core.Models;
using ACATSoci.Models;
using TestGame.Core;
using TestGame.Core.Commands;
using Path = System.IO.Path;

namespace TestGame
{
  public class MainWindowViewModel : SuperSimpleModelBase
  {
    public RelayCommand TestCommand1 { get; set; }
    public RelayCommand TestCommand2 { get; set; }
    public RelayCommand AddItemCommand { get; set; }
    public RelayCommand ClearCommand { get; set; }

    public ObservableCollection<string> TheItems { get; set; }


    private string _textToAdd;
    public string TextToAdd
    {
      get => _textToAdd;
      set => _textToAdd = value;
    }

    public Excel Excel { get; set; }
    public double DoubleValue
    {
      get => Get<double>();
      set => Set(value);
    }

    public double DoubleValue2
    {
      get => Get<double>();
      set => Set(value);
    }

    public bool IsMyControlEnabled
    {
      get => Get<bool>();
      set => Set(value);
    }


    public ObservableCollection<WorksheetModel> WorkSheetsCollection  
    {
      get => Get<ObservableCollection<WorksheetModel>>();
      set => Set(value);
    }

    public MainWindowViewModel()
    {
      DoubleValue = 123.56;
      DoubleValue2 = 3.1435356363;
      TestCommand1 = new RelayCommand(() => MessageBox.Show(DoubleValue.ToString(CultureInfo.InvariantCulture)));
      TestCommand2 = new RelayCommand(() => MessageBox.Show(DoubleValue2.ToString(CultureInfo.InvariantCulture)));
      AddItemCommand = new RelayCommand(() =>
      {
        TheItems.Add(TextToAdd);
        TextToAdd = string.Empty;
      });
      ClearCommand = new RelayCommand(() => TheItems.Clear());
      TheItems = new ObservableCollection<string> { @"Test", @"Donald", @"Pluto", @"Minnie", @"Jenny" };

      Excel = new Excel();

      var appPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
      try
      {
        Excel.Open( Path.Combine(appPath ?? throw new InvalidOperationException(), @"elenco_iscritti_acat_torino_est.xlsx"));
        Excel.ActivateWorksheet(@"TOTALE");

        var worksheets = Excel.GetAvailableWorksheets();
        if (worksheets != null)
        {
          WorkSheetsCollection = new ObservableCollection<WorksheetModel>();
          var i = 0;
          foreach (var worksheet in worksheets)
          {
            WorkSheetsCollection.Add(new WorksheetModel(Excel, worksheet, i++));
          }
        }
      }
      catch (Exception e)
      {
        Console.WriteLine(e);
      }
    }
  }
}
